# utils #

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](http://www.apache.org/licenses/LICENSE-2.0)

A collection of Java utility classes, that may be helpful in everyday programming.

## License ##

Code is under the [Apache Licence 2.0](http://www.apache.org/licenses/LICENSE-2.0).